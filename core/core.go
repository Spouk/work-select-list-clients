package core

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	_ "github.com/nakagami/firebirdsql"
	"log"
	"os"
	"time"
)

type RecordDatabase struct {
	Pcode    int64
	Bdate    string
	Fullname string
	Phone1   string
	Phone3   string
}
type RecordFile struct {
	Fullname string
	Bdate    string
	Dsn      string //dsn for dbs connecting
}
type Requester struct {
	fileCSV          string
	fileOUTCSV_FD    string
	fileOUTCSV_PCODE string
	FHandlerCSV      *os.File
	FHandlerOutFull  *os.File
	FHandlerOutPCODE *os.File
}

func NewRequester(inf, outf, outfcode string) (*Requester, error) {
	r := &Requester{}
	r.fileCSV = inf
	r.fileOUTCSV_FD = outf
	r.fileOUTCSV_PCODE = outfcode

	for i, x := range []string{inf, outf, outfcode} {
		tmpFh := &os.File{}
		if _, err := os.Stat(x); os.IsNotExist(err) {
			ff, err := os.Create(x)
			if err != nil {
				log.Println(err)
				return nil, err
			}
			tmpFh = ff
		} else {
			ff, err := os.OpenFile(x, os.O_EXCL|os.O_RDWR, 777)
			if err != nil {
				log.Println(err)
				return nil, err
			}
			tmpFh = ff
		}
		switch i {
		case 0:
			r.FHandlerCSV = tmpFh
		case 1:
			r.FHandlerOutFull = tmpFh
		case 2:
			r.FHandlerOutPCODE = tmpFh
		}
	}
	return r, nil
}
func (r *Requester) ReadCSV(pauseEnable bool, timing int, dsn string) ([]RecordFile, error) {
	red := csv.NewReader(r.FHandlerCSV)
	red.Comma = ','
	stock := []RecordFile{}
	records, err := red.ReadAll()
	if err != nil {
		log.Println(err)
		return stock, err
	}
	for cc, x := range records {
		nr := RecordFile{
			Fullname: x[0],
			Bdate:    x[1],
			Dsn:      dsn,
		}
		stock = append(stock, nr)
		for _, a := range `-\|/` {
			fmt.Printf("\r%c....[%4d:%4d]", a, cc, len(records))
			if pauseEnable {
				time.Sleep(time.Microsecond * time.Duration(timing))
			}
		}
	}
	return stock, nil
}

//manager for pool workers
func (c *Requester) Runer(countWorker int, dsn string) {
	var (
		chCommand     = make(chan bool)
		chWork        = make(chan RecordFile)
		chRdy         = make(chan RecordDatabase, 100)
		chToWork      = make(chan RecordFile)
		chEndRecords  = make(chan bool)
		chEndWork     = make(chan bool)
		counterWorker int
	)

	for x := 1; x <= countWorker; x++ {
		go c.worker(x, chWork, chCommand, chRdy, chEndWork)
	}

	writerPCODE := csv.NewWriter(c.FHandlerOutPCODE)
	writerFULL := csv.NewWriter(c.FHandlerOutFull)
	records, err := c.ReadCSV(false, 0, dsn)

	go func(records []RecordFile, chToWork chan RecordFile, chEndRecords chan bool) {
		for _, x := range records {
			chToWork <- x
		}
		chEndRecords <- true
	}(records, chToWork, chEndRecords)

	if err != nil {
		log.Fatal(err)
	}

	for {
		select {
		case r := <-chRdy:
			if err := writerFULL.Write([]string{fmt.Sprintf("%d", r.Pcode), r.Fullname, r.Bdate, r.Phone1, r.Phone3}); err != nil {
				log.Printf("error write csv full `%v`\n", err)
			} else {
				writerFULL.Flush()
			}

			if err := writerPCODE.Write([]string{fmt.Sprintf("%d", r.Pcode)}); err != nil {
				log.Printf("error write csv pcode `%v`\n", err)
			} else {
				writerPCODE.Flush()
			}

		case newW := <-chToWork:
			chWork <- newW

		case _ = <-chEndWork:
			if counterWorker == countWorker {
				break
			} else {
				counterWorker++
			}
		case _ = <-chEndRecords:
			log.Printf("--end list records--,exit work\n")
			close(chCommand)
			return
		}
	}
}
func (c *Requester) worker(id int, chWork chan RecordFile, chCommand chan bool, chRdy chan RecordDatabase, chEnd chan bool) {
	defer func() {
		log.Printf("worker #%v end working\n", id)
		chEnd <- true
		return
	}()
	for {
		select {
		case w := <-chWork:
			d, err := sql.Open("firebirdsql", w.Dsn)
			if err != nil {
				log.Printf("error connection db [#%d] `%v`\n", id, err)
				continue
			} else {
				row := d.QueryRow(fmt.Sprintf("select pcode, fullname,bdate, phone1, phone3 from fucker where bdate = '%v' and fullname containing '%v'", w.Bdate, w.Fullname))
				var res RecordDatabase
				if err = row.Scan(&res.Pcode, &res.Fullname, &res.Bdate, &res.Phone1, &res.Phone3); err != nil {
					log.Printf("[#%d] error scan `%v`\n", id, err)
				} else {
					chRdy <- res
				}
				d.Close()
			}

		case _ = <-chCommand:
			return
		}
	}
}
