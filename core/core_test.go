package core

import (
	"os"
	"strings"
	"testing"
)

func TestNewRequester(t *testing.T) {
	cd, err := os.Getwd()
	if err != nil {
		t.Error(err)
	}
	stock := []string{}
	for _, x := range []string{"testfile.csv", "outf.csv", "outpoc.csv"} {
		stock = append(stock, strings.Join([]string{cd, x}, "/"))
	}
	r, err := NewRequester(stock[0], stock[1], stock[2])
	if err != nil {
		t.Fatal(err)
	}
	dsn := "sysdba:sysdba@localhost:3055/tester"
	r.Runer(400, dsn)
}
