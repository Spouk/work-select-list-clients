package generator

import (
	"fmt"
	"testing"
)

func TestNewGenerator(t *testing.T) {
	g := NewGenerator()
	gg := g.Generate(40000, 20)
	fmt.Printf("Res: %v\n", gg.tmpGenerateResult)
	err := gg.ToCSV("testfile.csv")
	if err != nil {
		t.Error(err)
	}
	dsn := "sysdba:sysdba@localhost:3055/tester"
	err = g.PushToDatabase(dsn)
	if err != nil {
		t.Error(err)
	}
}
